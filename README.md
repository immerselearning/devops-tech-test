# Immerse Learning: DevOps Tech Test

## Task 1

There is a directory of images in this repository.

Write a script in the language of your choice to perform the following:

* For each image, create a thumbnail of 10x10 pixels in the jpeg format
   without changing the original files.
* Add "_thumbnail" to the filename of the new images.
* Upload the new images to the "public.development.immerseplatform.com"
   S3 bucket. You will be provided an IAM access key to do so.

__Reference:__

You may wish to use ImageMagick to perform image manipulation.

A guide is available here:
https://www.brianlinkletter.com/process-images-for-your-blog-with-imagemagick/

- - -

## Task 2

In the `lambda_endpoint` directory of this project, there is an `index.js` file.
Write a test using the `mocha` library for this lambda function.

- - -

## Task 3

There is an imaginary business with the following:

* A public web frontend application built in Angular which records short
  text messages for users.
* A very old java application which processes these messages and
  finally stores them in redis.
* An internal dashboard for staff to view internal metrics, built in Angular.
* A simple javascript application built in NodeJS which provides the
  data for the dashboard by connecting to redis.

How would you architect these applications in AWS?

What measures would you take to ensure this architecture was secure?

What would you do to ensure the internal dashboard was secure?

What would you do to ensure the public-facing application could scale?

If the business wanted to deprecate their old java application, what
steps would you recommend to migrate to a different technology, and
which technology would you recommend?

- - -

## Possible Questions

* What is your favorite service provided by AWS and why?
* What is the most interesting project you've worked on in the recent
  past?
* Explain how knowledge silos come about and what can be done to
  prevent them.
* Why is CI/CD important?
* What is your favorite tool that you use in your job and why?
* What are the different type of load balancers AWS provides, and
  explain where and why you'd use each one.
* Are you more inclined to use a managed database solution, or to
  deploy your own? Why?
* What are some core principles which you stick to which ensure you are
  productive and valuable in you role?
* Explain what a `git rebase` command does.
* Explain the difference between a "unit" test and an "acceptance" or
  "end-to-end" test. How does this change the way you write those
  tests?
* What do you think are some of the potential issues with "code review",
  and what are some potential fixes for these issues?
* Explain your use of "code review" or "pull requests" in the past, and
  provide an example of how you've improved that process for developers.
