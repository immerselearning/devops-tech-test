function handler(event, context, callback) {
  const response = {
    "statusCode": 200,
    "headers": { "Content-Type" : "text/csv" },
    "body": "TEST,DATA",
    "isBase64Encoded": false
  }

  callback(null, response);
}

module.exports = { handler }
